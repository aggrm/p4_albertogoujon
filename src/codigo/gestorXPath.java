/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.*;

/**
 *
 * @author Alberto Goujon
 */
public class gestorXPath {

    Document doc = null;                                                        //Variable global para poder trabajar con el archivo 

    public int abrir_xml_con_dom(File fichero) {
        try {
            //Crea un objeto DocumentBuilderFactory para el DOM (JAXP)
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();//Crea un objeto de DocumentBuilderFactory
            factory.setIgnoringComments(true);                                  //El modelo DOM no debe contemplar los comentarios que tengo el xml
            factory.setIgnoringElementContentWhitespace(true);                  //Ignora los espacios en blanco que tenga el documento
            DocumentBuilder builder = factory.newDocumentBuilder();             //Crea un objeto DocumentBuilder cargar en él le estructura de árbol DOM a partir del XLM seleccionado
            doc = builder.parse(fichero);                                       //Interpreta el documento XML y genera el DOM equivalente
            return 0;                                                           //Ahora doc al árbol DOM listo para ser recorrido    
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

    }

    public String ejecutaXPath(String consulta) {
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();                //Crea el objeto XPath
            XPathExpression exp = xpath.compile(consulta);                      //Crea un XPathExpression con la consulta deseada

            Object result = exp.evaluate(doc, XPathConstants.NODESET);          //Ejecuta la consulta indicando que se ejecute sobre el DOM y que devolverá el resultado como una lista de nodos.
            NodeList nodeList = (NodeList) result;
            
            //Ahora recorrere la lista para sacar los resultados
            String salida = "";
            String datos_nodo[];
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node ntemp = nodeList.item(i);                                  //Para consulta de Libro
                if(ntemp.getNodeName() == "Libro")
                {
                    datos_nodo = procesarLibro(ntemp);                          //Obtengo la lista del metodo ya con todos los datos de ese (<Libro>)
                    //Es lo que voy a imprimir por el jTextArea
                    salida = salida + "\n" + "Publicado en: " + datos_nodo[0];
                    salida = salida + "\n" + "El título es: " + datos_nodo[1];
                    salida = salida + "\n" + "El autor es: " + datos_nodo[2];
                    salida = salida + "\n" + "La editorial es: " + datos_nodo[3];
                    salida = salida + "\n -----------------------";
                }
                else
                {
                    salida = salida + "\n"
                        + nodeList.item(i).getChildNodes().item(0).getNodeValue() + "\n";
                }
                
            }
            System.out.println(salida);

            return salida;
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
            return "No se pudo realizar la consulta";
        }
    }

    protected String[] procesarLibro(Node n) {
        String datos[] = new String[4];                                         //Lista con la informacon de los libros
        Node ntemp = null;                                                      //Nodos temporales
        int contador = 1;                                                       //Contador para poder obtener titulo, autor y editoral

        datos[0] = n.getAttributes().item(0).getNodeValue();                    //Obtiene publicado_en, además lo guardo en la posicion 0 en la lista de datos
        NodeList nodos = n.getChildNodes();                                     //Obtiene los hijos del Libro (titulo, autor y editorial)

        for (int i = 0; i < nodos.getLength(); i++) {
            ntemp = nodos.item(i);                                              //nos encontramos en la etiqueta (<Libro>)
            if (ntemp.getNodeType() == Node.ELEMENT_NODE) {
                datos[contador] = ntemp.getFirstChild().getNodeValue();         //Acedo al nodo tipo texto que es el que contiene el valor del título del libro
                contador++;
            }
        }
        return datos;
    }

    private static String convertStringArrayToString(String[] strArr, String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (String str : strArr) {
            sb.append(str).append(delimiter);
        }
        return sb.substring(0, sb.length() - 1);
    }
}
