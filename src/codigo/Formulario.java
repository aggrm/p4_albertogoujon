/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Alberto Goujon
 */
public class Formulario extends javax.swing.JFrame {

    gestorXPath gesXPATH = new gestorXPath();
    /**
     * Creates new form Formulario
     */
    public Formulario() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setTitle("Gestores XPATH Alberto Goujon");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        abrirXPATH = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        textoCambiante = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        campoConsulta = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        resultadosDeConsultas = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        abrirXPATH.setText("Abrir XPath");
        abrirXPATH.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                abrirXPATHMousePressed(evt);
            }
        });

        jButton2.setText("Ejecutar consulta");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton2MousePressed(evt);
            }
        });

        textoCambiante.setText("Abre el XPATH");

        campoConsulta.setColumns(20);
        campoConsulta.setRows(5);
        jScrollPane2.setViewportView(campoConsulta);

        resultadosDeConsultas.setEditable(false);
        resultadosDeConsultas.setColumns(20);
        resultadosDeConsultas.setRows(5);
        jScrollPane3.setViewportView(resultadosDeConsultas);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(abrirXPATH, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(textoCambiante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jScrollPane2)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 117, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(abrirXPATH)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(textoCambiante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void abrirXPATHMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_abrirXPATHMousePressed
        File fichero = menuGuardar();
        if(gesXPATH.abrir_xml_con_dom(fichero) == 0)
        {
           gesXPATH.abrir_xml_con_dom(fichero);
           textoCambiante.setText("¡Ha cargado correctamente la estructura de XPATH!");
        }
        else
        {
            //------------------------Mensaje Error-----------------------------
            String message = "Carga un archivo para la estructura XPATH";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                    JOptionPane.ERROR_MESSAGE);
            //------------------------------------------------------------------
            System.out.println("¡No ha cargado la estructura!");
        }
    }//GEN-LAST:event_abrirXPATHMousePressed

    private void jButton2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MousePressed
        
        String consultaRealizada = campoConsulta.getText();                     //Obtengo la consulta que realiza el usuario
        System.out.println(consultaRealizada);
        
        if(gesXPATH.ejecutaXPath(consultaRealizada) != "No se pudo realizar la consulta")
        {
            resultadosDeConsultas.setText("");
            textoCambiante.setText("¡Se pudo realizar la consulta!");
            resultadosDeConsultas.setText(gesXPATH.ejecutaXPath(consultaRealizada));
        }
        else if(resultadosDeConsultas.equals(""))
        {
            //------------------------Mensaje Error-----------------------------
            String message = "Consulta nula";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
               JOptionPane.QUESTION_MESSAGE);
            //------------------------------------------------------------------ 
        }
        else 
        {
            resultadosDeConsultas.setText("");
            //------------------------Mensaje Error-----------------------------
            String message = "Realiza correctamente la consulta XPATH";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
               JOptionPane.ERROR_MESSAGE);
            //------------------------------------------------------------------
            resultadosDeConsultas.setText(gesXPATH.ejecutaXPath(consultaRealizada));
            textoCambiante.setText("¡No se pudo realizar la consulta!");
        }    
    }//GEN-LAST:event_jButton2MousePressed
    
    private File menuGuardar() 
    {
        File fichero = null;
        int rv;

        JFileChooser fc = new JFileChooser();
        fc.setMultiSelectionEnabled(false);
        fc.setDialogType(JFileChooser.OPEN_DIALOG);
        rv = fc.showOpenDialog(this);

        try {
            if (rv == JFileChooser.APPROVE_OPTION) {
                fichero = fc.getSelectedFile();
            }
        } catch (Exception e) {

            if (rv == JFileChooser.CANCEL_OPTION) {
                System.out.println(rv);
            }
        }

        return fichero;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Formulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Formulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Formulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Formulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Formulario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton abrirXPATH;
    private javax.swing.JTextArea campoConsulta;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea resultadosDeConsultas;
    private javax.swing.JLabel textoCambiante;
    // End of variables declaration//GEN-END:variables
}
